FROM haproxytech/haproxy-alpine:2.4.7

RUN apk --update add bash curl jq coreutils && \
    curl -sLo /usr/local/bin/websocat https://github.com/vi/websocat/releases/download/v1.9.0/websocat_linux64 && \
    chmod +x /usr/local/bin/websocat
